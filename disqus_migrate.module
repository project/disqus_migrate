<?php

/**
 * @file
 * Add functionality to export comments as XML
 */

/**
 * Implements hook_help().
 */
function disqus_migrate_help($path, $arg) {
  switch ($path) {
    case 'admin/config/services/disqus/export':
      return '<p>' . t(
        'When you are ready to perform an export, visit !thispage.',
        array(
          '!thispage' => l(t('this page'), 'admin/content/comment/disqus_export'),
        )
      ) . '</p>';
    case 'admin/content/comment/disqus_export':
      return '<p>' . t(
        'Exporting via XML will just gather all of your websites comments and format them for importing manually into Disqus.',
        array(
          '!exportsettings' => l(t('export settings'), 'admin/settings/disqus/export'),
          '!mainsettings' => l(t('main settings'), 'admin/settings/disqus'),
        )
      ) . '</p>';
  }
}

/**
 * Implements hook_menu().
 */
function disqus_migrate_menu() {
  $items = array();
  $items['admin/config/services/disqus/export'] = array(
    'title' => 'Export',
    'description' => 'Settings for exporting comments from Drupal into Disqus',
    'access arguments' => array('administer disqus'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('disqus_migrate_admin_export_settings'),
    'file' => 'include/disqus_migrate.export.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/content/comment/disqus_export'] = array(
    'title' => 'Disqus Export',
    'description' => 'Export comments from the Drupal to Disqus.',
    'access arguments' => array('administer disqus'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('disqus_migrate_admin_export'),
    'file' => 'include/disqus_migrate.export.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;

}

/**
 * Implements hook_form_disqus_admin_settings_alter().
 */
function disqus_migrate_form_disqus_admin_settings_alter(&$form, &$form_state, $form_id) {
  $form['migrate'] = array(
    '#type' => 'fieldset',
    '#title' => t('Migrate'),
    '#group' => 'settings',
  );

  $form['migrate']['disqus_migrate_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Overide base URL'),
    '#default_value' => variable_get('disqus_migrate_base_url'),
    '#description' => t('Overide the base URL. Usefull if generating XML file for a different domain from current (http://%s). Use the full base url eg. http://www.somesite.co.uk', array('%s' => $_SERVER['HTTP_HOST'])),
  );
}
